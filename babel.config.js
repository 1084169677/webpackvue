/*
 * @Author: huangfucheng
 * @Date: 2021-08-04 17:45:16
 * @LastEditors: huangfucheng
 * @LastEditTime: 2021-08-31 16:16:25
 * @Description:
 */
productionPlugins.push([
  'transform-remove-console',
  {
    exclude: ['error', 'warn']
  }
])
module.exports = {
  presets: [
    [
      "@vue/app",
      '@babel/preset-env',
      {
        targets: {
          browsers: ['last 2 versions'] // 最近 2 个版本的浏览器
        }
      },
      'import',
      {
        libraryName: 'element-plus',
        customStyleName: name => {
          name = name.slice(3)
          return `element-plus/packages/theme-chalk/src/${name}.scss`
        }
      }
    ]
  ],
  plugins: [
    [
      'component',
      {
        libraryName: 'element-plus'
      }
    ],
    '@babel/plugin-syntax-dynamic-import'
  ]
}
