/*
 * @Author: huangfucheng
 * @Date: 2021-08-04 17:00:11
 * @LastEditors: huangfucheng
 * @LastEditTime: 2021-10-15 16:03:34
 * @Description:
 */
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
// 最新的 vue-loader 中，VueLoaderPlugin 插件的位置有所改变
const { VueLoaderPlugin } = require('vue-loader/dist/index')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
// const MiniCssExtractPlugin = require('mini-css-extract-plugin')
// const extractTextPlugin = require("extract-text-webpack-plugin")
module.exports = {
  mode: 'development',
  entry: path.resolve(__dirname, './src/main.js'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/[name].js',
    publicPath: './'
  },
  resolve: {
    // 配置 解析模块路径别名：可简写路径。
    alias: {
      '@': path.resolve(__dirname, 'src')
    }
    // 配置 省略文件路径的后缀名。默认省略js和json。也是webpack默认认识的两种文件类型
    // extensions: ['.js', '.json', '.css'] // 新加css文件
    // 该配置明确告诉webpack，直接去上一层找node_modules。
    // modules: [path.resolve(__dirname, "../node_modules")],
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: ['vue-loader']
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(scss|sass)$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.js$/,
        exclude: /node_modules/, // 不编译node_modules下的文件
        loader: 'babel-loader'
      },
      {
        //处理图片资源
        test: /\.(jpg|png|gif|webp)$/,
        type: 'asset',
        generator: {
          // 输出文件位置以及文件名
          filename: 'images/[name][ext]'
        },
        parser: {
          dataUrlCondition: {
            maxSize: 10 * 1024 //超过10kb不转base64
          }
        }
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './index.html'),
      filename: 'index.html'
      // title: '123'
    }),
    // 添加 VueLoaderPlugin 插件
    new VueLoaderPlugin(),
    new CleanWebpackPlugin()
  ],
  devServer: {
    contentBase: path.resolve(__dirname, './dist'),
    port: 9528,
    publicPath: '/'
  }
}
