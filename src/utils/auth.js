/*
 * @Author: huangfucheng
 * @Date: 2021-08-17 15:15:29
 * @LastEditors: huangfucheng
 * @LastEditTime: 2021-10-12 09:45:54
 * @Description: 设置登录权限/cookies
 */
import Cookies from 'js-cookie'

const TokenKey = 'user_token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
} // 设置唯一token存进cookies,保证登录用户身份唯一性

export function removeToken() {
  return Cookies.remove(TokenKey)
}
