// 对axios的二次封装
import axios from 'axios'
import baseurl from './urlConfig.js'
// 封装公用的，每个实例有单独自己的拦截器，防止某些请求有特殊要求
class Http {
  constructor() {
    this.timeout = 1200000 // 超时时间，防止后端意外暂时不设置
    this.baseURL = baseurl
    this.headers = {
      'Content-Type': 'application/json; charset=UTF-8',
      // 'Content-Type': 'application/x-
      // www-form-urlencoded',
      'X-Requested-With': 'XMLHttpRequest',
      'Cache-Control': 'no-store',
      'prefer-service-zone': '',
      Pragma: 'no-cache'
    }
  }

  mergeOptions(options) {
    // 合并参数
    return {
      timeout: this.timeout,
      baseURL: this.baseURL,
      headers: this.headers,
      options
    }
  }

  request(options) {
    // 请求设置拦截器
    // 传入参数+默认参数
    const opts = this.mergeOptions(options)
    const axiosInstance = axios.create() // axios()
    // 添加拦截器
    // this.setInterceptor(axiosInstance, opts.url)
    return axiosInstance(opts)
  }

  get(url, config = {}) {
    return this.request({
      url,
      method: 'get',
      config
    })
  }

  post(url, data) {
    // 对data进行格式化在此处
    return this.request({
      url,
      method: 'post',
      data
    })
  }

  upload(url, data) {
    // 对data进行格式化在此处
    return this.request({
      url,
      method: 'post',
      data,
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  }
}
export default new Http()
