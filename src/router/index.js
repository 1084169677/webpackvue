import { createRouter, createWebHashHistory } from 'vue-router'
import { getToken } from '@/utils/auth'
import { ElMessage } from 'element-plus'

import Home from '@/views/Home.vue'
import Login from '@/views/Login/Login-swipe.vue'
import notFound from '@/views/404.vue'
import layout from '@/Layout/index.vue'
import AppMain from '@/Layout/components/AppMain.vue'
import addShop from '@/views/shop/addShop.vue'
import shopList1 from '@/views/shop/shopList.vue'
import shopList2 from '@/views/shop/shopListTest.vue'
const routerHistory = createWebHashHistory()

const router = createRouter({
  history: routerHistory,
  routes: [
    {
      path: '/login',
      component: Login,
      hidden: true
    },
    // 首页
    {
      path: '/',
      component: layout,
      children: [
        {
          path: '/home',
          component: Home,
          meta: { title: '首页', icon: 'el-icon-s-home', isShow: true }
        }
      ]
    },
    // 商品
    {
      path: '/shop',
      component: layout,
      meta: {
        title: '商品',
        icon: 'el-icon-pie-chart'
      },
      children: [
        {
          path: '/shop/shopList',
          component: AppMain,
          meta: { title: '商品列表', icon: 'el-icon-menu', isShow: true },
          children: [
            {
              path: '/shop/shopList/test1',
              component: shopList1,
              meta: { title: '商品列表测试', icon: 'el-icon-s-marketing', isShow: true }
            },
            {
              path: '/shop/shopList/test2',
              // component: AppMain,
              component: shopList2,
              meta: { title: '商品列表测试2', icon: 'el-icon-s-custom', isShow: true },
              // children:[
              //   {
              //     path: '/shop/shopList/test2/list1',
              //     component: shopList2,
              //     meta: { title: '商品列表测试2下的1', icon: 'el-icon-s-marketing', isShow: true }
              //   },
              //   {
              //     path: '/shop/shopList/test2/list2',
              //     component: shopList2,
              //     meta: { title: '商品列表测试2下的2', icon: 'el-icon-s-marketing', isShow: true }
              //   },
              // ]
            }
          ]
        },
        {
          path: '/shop/addshop',
          component: addShop,
          meta: { title: '添加商品', icon: 'el-icon-s-claim', isShow: true }
        }
      ]
    },
    //notFound
    {
      path: '/',
      component: layout,
      hidden: true, //将404放进route中，但不展示
      children: [
        {
          path: '/404',
          component: notFound,
          meta: { isShow: false }
        }
      ]
    },
    { path: '/:pathMatch(.*)', redirect: '/404', hidden: true }
  ]
})
router.beforeEach((to, from, next) => {
  // 1.如果访问的是登录页面（无需权限），直接放行
  if (to.path === '/login') return next()
  // 2.如果访问的是有登录权限的页面，先要获取token
  const tokenStr = getToken('user_token')
  // 2.1如果token为空，强制跳转到登录页面；否则，直接放行
  if (!tokenStr || tokenStr == 'null' || tokenStr == undefined) {
    ElMessage({
      message: '用户信息已失效！请重新登录',
      type: 'error'
    })
    return next('/login')
  }
  next()
})

export default router
