/*
 * @Author: huangfucheng
 * @Date: 2021-09-30 16:59:16
 * @LastEditors: huangfucheng
 * @LastEditTime: 2021-10-12 13:46:02
 * @Description:
 */
/** 全部接口 */
import axios from '@/utils/axios'
// 登录
export const toLogin = params => axios.post('/ebuy/login', params)
