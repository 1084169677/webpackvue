/*
 * @Author: huangfucheng
 * @Date: 2021-08-04 16:59:36
 * @LastEditors: huangfucheng
 * @LastEditTime: 2021-08-17 14:01:36
 * @Description: 入口文件
 */

import { createApp } from 'vue' // Vue 3.x 引入 vue 的形式
import App from './App.vue' // 引入 APP 页面组建
import router from './router/index'
import store from './store'
import ElementPlus from 'element-plus'
import 'element-plus/lib/theme-chalk/index.css'
// import ElementUI from 'element-ui';
// import 'element-ui/lib/theme-chalk/index.css';
// import  VueResource  from 'vue-resource'
createApp(App).use(router).use(store).use(ElementPlus).mount('#root') // 通过 createApp 初始化 app
