import { toLogin } from '@/api/common'
import { setToken, getToken } from '@/utils/auth'
// import axios from 'axios'
const TokenKey = 'user_token'
const getDefaultState = () => {
  return {
    token: getToken(),
    name: '',
    avatar: ''
  }
}
const state = getDefaultState()

const mutations = {
  RESET_STATE: state => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { userName, passWord } = userInfo
    return new Promise((resolve, reject) => {
      // const param = {
      //   userName: userName,
      //   passWord: passWord
      // }
      // toLogin(param)
      //   .then(res => {
      //     console.log(res)
      //     resolve('登录成功')
      //     commit('SET_TOKEN', userName)
      //     setToken(userName)
      //   })
      //   .catch(err => {
      //     console.log(err)
      //     reject('用户名或密码错误!')
      //   })
      if (userName == 'admin' && passWord == '123456') {
        resolve('登录成功')
        commit('SET_TOKEN', userName)
        setToken(userName)
      } else if (userName != 'admin' || passWord != '123456') {
        reject('用户名或密码错误!')
      }
    })
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
